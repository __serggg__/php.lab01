<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header class="header">
        <a href="https://mospolytech.ru/"><img src="logo.png" alt=""></a>
        <p class="zadan">Задание №1</p>
    </header>
    <main>
        <?php
        echo "Hello, world!"
        ?>
    </main>
    <footer>
    Задание для самостоятельной работы «Hello, World!» Создать веб-страницу с динамическим контентом. Загрузить код в удаленный репозиторий. Залить на хостинг. Дано: Header = слева логотип МосПолитеха, по центру название работы. Footer = задание для самостоятельно работы (без описания). Main = любой html-элемент с адекватным динамическим контентом (пример Hello, World). Ответ на гугл диск: Ссылка на репозиторий URL – адрес страницы.
    </footer>
</body>
</html>